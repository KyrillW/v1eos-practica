#include "shell.hh"

int main()
{ std::string input;

  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand
  char prompt[1];
  int fd = syscall(SYS_open, "Plopjes.txt", O_RDONLY, 0755);
  while(syscall(SYS_read, fd, prompt, 1)){
		std::cout << prompt;
  }
  

  while(true)
  { std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file(){ // ToDo: Implementeer volgens specificatie.
	std::string input = "";
	std:: string naam = "";
	bool eof = true;
	std::cout << "Geef een bestandsnaam: ";
	std::getline(std::cin, naam);
	std::string enter = "\n";
	int fd = syscall(SYS_open, naam.c_str(), O_RDWR | O_CREAT, S_IRWXU);
	while(eof){
		std::cout << "Geef je tekst: ";
		std::getline(std::cin, input);
		if(input.find("<EOF>") != std::string::npos){
			syscall(SYS_write, fd, input.c_str(), input.size()-5);
			syscall(SYS_write, fd, enter.c_str(), enter.size());
			break;
		}
		else{
			syscall(SYS_write, fd, input.c_str(), input.size());
			syscall(SYS_write, fd, enter.c_str(), enter.size());
		}
	}
}

void list(){ // ToDo: Implementeer volgens specificatie.
	int pid = syscall(SYS_fork);
	if(pid == 0){
		const char *args[] = {"/bin/ls", "-la", (char *) 0};
		syscall(SYS_execve, "/bin/ls", args, 0);
		syscall(SYS_exit, 0);
	}
	else{
		syscall(SYS_wait4, pid, 0, 0, 0);
	}
}

void find(){ // ToDo: Implementeer volgens specificatie.
	int pfd[2];
	syscall(SYS_pipe, pfd);
	std::string zoek = "";
	std::cout << "Geef een string om te vinden: ";
	std::getline(std::cin, zoek);
	int pid_1 = syscall(SYS_fork);
	
	if(pid_1 == 0){
		const char *args1[] = {"/usr/bin/find", ".", (char *) 0};
		syscall(SYS_dup2, pfd[1], 1);
		syscall(SYS_close, pfd[0]);
		syscall(SYS_close, pfd[1]);
		syscall(SYS_execve, "/usr/bin/find", args1, 0);
	}
	else{
		syscall(SYS_wait4, pid_1, 0, 0, 0);
	}
	
	int pid_2 = syscall(SYS_fork);
	
	if(pid_2 == 0){
		const char *args2[] = {"/bin/grep", zoek.c_str(), (char *) 0};
		syscall(SYS_dup2, pfd[0], 0);
		syscall(SYS_close, pfd[0]);
		syscall(SYS_close, pfd[1]);
		syscall(SYS_execve, "/bin/grep", args2, 0);
	}
	else{
		syscall(SYS_wait4, pid_2, 0, 0, 0);
	}
}

void seek(){ // ToDo: Implementeer volgens specificatie.
	int fd2 = syscall(SYS_open, "loop", O_RDWR | O_CREAT, S_IRWXU);
	syscall(SYS_write, fd2, "x", 1);
	for(unsigned int i = 0; i < 5000000; i++){
		syscall(SYS_write, fd2, "\0", 1);
	}
	syscall(SYS_write, fd2, "x", 1);
	
	int fd1 = syscall(SYS_open, "seek", O_RDWR | O_CREAT, S_IRWXU);
	
	syscall(SYS_write, fd1, "x", 1);
	syscall(SYS_lseek, fd1, 5000000, 1);										//Seek is sneller.
	syscall(SYS_write, fd1, "x", 1);												//ls -lh geeft zelfde grootte.
}																					//ls -lS geeft dat seek 1 Byte kleiner is dan loop.

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
