#include <iostream>
#include <string>
using namespace std;

string translate(string line, string argument){
	int rotation = stoi(argument);
	for(unsigned int i = 0; i < line.size(); i++){
		if(line[i] < 97){
			if(line[i] + rotation > 90){
				int extra = line[i] + rotation - 90;
				line[i] = 64 + extra;
			}
			else{
				line[i] = line[i] + rotation;
			}
		}
		else{
			if(line[i] + rotation > 122){
				int extra = line[i] + rotation - 122;
				line[i] = 96 + extra;
			}
			else{
				line[i] = line[i] + rotation;
			}
		}
	}
	string result = line; // implementeer dit
	return result;
}

int main(int argc, char *argv[]){
	string line;
	
		if(argc != 2){
			cerr << "Deze functie heeft exact 1 argument nodig" << endl;
			return -1;
		}

	while(getline(cin, line)){
		cout << translate(line, argv[1]) << endl;
	} 
	
	return 0;
}
